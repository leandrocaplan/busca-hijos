package familia;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Vista {

    private Modelo modelo;
    private JFrame frame;
    private JPanel panel;
    private JLabel padre;
    private JLabel hijos;
    private JLabel numHijo;
    private JTextArea textAreaHijos;
    private JTextField textFieldPadre;
    
    private JButton botonBuscarProgenitor;
    
    //Al constructor de la vista le paso una referencia al modelo
    public Vista(Modelo modelo) {
        this.modelo = modelo;
        //Le paso un exceptionListener al modelo
        this.modelo.addExceptionListener(new ExceptionListener());
        
        //En el constructor, hago el maquetado de la vista
        
        //Instancio los componentes de la vista
        frame = new JFrame();
        panel = new JPanel();
        panel.setLayout(new BoxLayout (panel, BoxLayout.Y_AXIS)); 
        textAreaHijos = new JTextArea();
        textAreaHijos.setColumns(30);
        textAreaHijos.setRows(12);
        //textAreaHijos.setSize(new Dimension(200,200));
        botonBuscarProgenitor = new JButton("Ingrese progenitor");
        textFieldPadre = new JTextField(26);
        padre= new JLabel("Padre");
        hijos= new JLabel("Hijos");
        frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(),BoxLayout.Y_AXIS));
        //frame.getContentPane().setLayout(new FlowLayout());
        
        numHijo= new JLabel("Numero de hijo");
        //Formateo el panel
        frame.getContentPane().setBackground(Color.CYAN);
        panel.setSize(600,500);
        frame.setSize(600,500);
        
        

        //Agrego al panel los botones y cajas de texto
        
        panel.add(padre);
        panel.add(textFieldPadre);
        panel.add(botonBuscarProgenitor);
        panel.add(hijos);
        panel.add(textAreaHijos);
        

        //Agrego el panel al frame
        frame.getContentPane().add(panel);
    }

    //Muestra el frame cargado con lo que instancié y agregué en el constructor
    public void mostrar() {
        //Pongo el título al frame
        frame.setTitle("Buscador de progenitores en base de datos genealogica");
        //Le digo al frame que cuando se cierre la ventana, salga del programa
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Formateo el tamaño del frame
        frame.pack();
        frame.setLocationRelativeTo(null);
        //Hago visible el frame
        frame.setVisible(true);
    }

    public String getTextAreaHijos() {
        return textAreaHijos.getText();
    }

    public void setTextAreaHijos(String nombre) {
        textAreaHijos.setText(nombre);
    }

    public String getTextFieldPadre() {
        return textFieldPadre.getText();
    }

    public void setTextFieldPadre(String nombre) {
        textFieldPadre.setText(nombre);
    }

    //Le paso un ActionListener al boton buscarProgenitor
    public void addBotonBuscarProgenitor(ActionListener al) {
        botonBuscarProgenitor.addActionListener(al);
    }

    public void mostrarExcepcion(String s) {
        JOptionPane.showMessageDialog(frame, s, "Error!", JOptionPane.ERROR_MESSAGE);
    }

    private class ExceptionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            mostrarExcepcion(event.getActionCommand());
        }
    }
}
