package familia;

//Importo las librerías de AWT y de SQL
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;

public class Modelo {

    //Declaro los atributos String que utilizaré para conectarme query la base de datos
    private String driver;
    private String prefijoConexion;
    private String ip;
    private String usr;
    private String psw;
    private String bd;

    //Declaro un string 'tabla' que utilizaré para consultar query la base de datos
    private String tabla;
 
    //Declaro dos strings donde me guardo el resultado de las consultas query la base de datos
    //private String resultadoHijoConsulta;
    //private String resultadoPadreConsulta;
   // private int numeroHijo;
    //Declaro un atributo de tipo Connection, que representará mi referencia query la base de datos
    private Connection connection;
    //Declaro un atributo de tipo ActionListener, que representa el evento que se activará 
    //cuando ocurra una excepción
    private ActionListener listener;
    private ArrayList<String> listaHijos;
    private String resultadoConsultaHijos;
    //Declaro un constructor vacío del modelo, donde inicializo algunos de sus atributos de tipo String
    public Modelo() {
        driver = "com.mysql.jdbc.Driver";
        prefijoConexion = "jdbc:mysql://";
        ip = "localhost";             // Direccion IP donde esta corriendo el SGBD
        usr = "";                     // Usuario
        psw = "";                     // Password
        bd = "familia";               // Base de datos
        tabla = "integrantes";         // Tabla de las estaciones
        listaHijos=new ArrayList();
        resultadoConsultaHijos = "";
    }

    //Hago los getters para el nombre de la estación donde estoy parado y su comentario
   
    public String getResultadoConsultaHijos() {
        return this.resultadoConsultaHijos;
    }
    //Codifico el método 'consultar', que se ejecutará cada vez que clickee el boton 'avanzar'
    //Este método recibe un String 'nombreBuscado' y me guarda en el atributo resultadoConsultaHijos todos los
    //nombres de los hijos de la persona ingresada. Devuelve true si encontró al menos un hijo, y false si no lo
    //encontró.
    public boolean consultar(String nombreBuscado) throws SQLException {
        //Declaro un flag que me indicará si la persona ingresada tiene al menos un hijo
        boolean encontrado=false;
      
        //Verifico que el nombre ingresado en la vista no esté vacío y que exista en la tabla
        if (!nombreBuscado.isEmpty() && existeNombre(nombreBuscado)) {
            //Reseteo la lista de hijos de esa persona
            listaHijos=new ArrayList();
            //Me guardo una referencia a la base de datos
            connection = obtenerConexion();
            //Hago un print que me indicará que la conexión fue hecha correctamente
            System.out.println(connection);

            //Creo el statement
            Statement statement = connection.createStatement();
            //Creo el resultSet y lo inicializo en null
            ResultSet resultSet = null;
            
            
            //Declaro el string donde genero la query
            String query;
            
            //Creo la query buscando al hijo mayor de la persona ingresada
            //Si no tiene hijo mayor, significa que no tiene hijos
            query = "SELECT HijoMayor"
                        + " FROM " + tabla
                        + " WHERE Nombre='" + nombreBuscado + "'";
            //Imprimo por pantalla la query
            System.out.println(query);
            //Ejecuto la query
            resultSet = statement.executeQuery(query);
            //Muestro por pantalla el resultado de la query
            Debug.imprimirResultadoQuery(resultSet, 1);
            
            //Si la query no devolvió nada o dió un resultado vacío, adelanto el resultSet y entro al if
            if(!resultSet.next() || resultSet.getString(1).equals("")){
                //Cierro el resultSet y el statement
                resultSet.close();
                statement.close();
                //Devuelvo falso, ya que no se encontró ningún hijo para la persona ingresada
                return false;
            }
            
            //Si la query me devolvió un resultado, entro al else
            else{
               //Me guardo en nombreBuscado el resultado de la query
                nombreBuscado=resultSet.getString(1);
                //listaHijos.add(nombreBuscado);
                //Bajo el flag indicando que se encontró un resultado
                encontrado=true;
            }
           // while(!nombreBuscado.equals("") && !ultimoHijo){
          
           //Mientras el string 'nombreBuscado' no esté vacío, sigo iterando en el while 
           //Corto el while cuando alguna query me dé un resultado vacío
           while(!nombreBuscado.equals("")){     
               //Genero una nueva query, buscando al hermano menor de la persona buscada
                query = "SELECT HermanoMenor"
                        + " FROM " + tabla
                        + " WHERE Nombre='" + nombreBuscado + "'";
                //Imprimo por pantalla la query
                System.out.println(query);
                //Ejecuto la query
                resultSet = statement.executeQuery(query);
                //Imprimo el resultado de la query
                Debug.imprimirResultadoQuery(resultSet, 1);
                //Si la query me dió algún resultado, adelanto el resultSet y entro al if
                if(resultSet.next()){
                    //Agrego el nombreBuscado a la lista de hijos
                    listaHijos.add(nombreBuscado);
                    //Muestro como me quedó hasta ahora la lista de hijos
                    Debug.imprimirLista(listaHijos);
                    //Piso el nombreBuscado con el resultado de la query, de manera que en el siguiente ciclo del while
                    //ejecuto la siguiente query con el resultado de esta.
                    nombreBuscado=resultSet.getString(1);
                }
            }
           //Me cargo en el atributo resultadoCOnsultaHijos la lista de hijos de la persona ingresada que obtuve
            cargarLista(listaHijos);
            //Cierro el resultSet y el statement
            resultSet.close();
            statement.close();
            //resultadoConsultaHijos=listaHijos.toString();
        }
        //Devuelvo el flag indicando si se encontró o no algún hijo para la persona ingresada.
        return encontrado;
    }
    //Esta función recibe un ArrayList y me lo carga en el atributo resultadoConsultaHijos
    public void cargarLista(ArrayList<String> listaHijos){
        resultadoConsultaHijos="";
        for(String hijo:listaHijos){
            this.resultadoConsultaHijos += (hijo + "\n");
        }
    }
    
    //Esta función me devuelve true si encontró el nombre buscado en la tabla, y false si no lo encontró
    public boolean existeNombre(String nombreBuscado) throws SQLException{
        System.out.println("Entro a existeNombre");
        //Genero la query, buscando el nombre ingresado en todos los campos
        String query= "SELECT * FROM " + tabla + " WHERE Nombre='" + nombreBuscado + "' OR HijoMayor='"
             + nombreBuscado + "' OR HermanoMenor='" + nombreBuscado + "'"   ;
        
        //Imprimo por pantalla la query
        System.out.println(query);
        //Realizo la conexión y creo el statement
        connection= obtenerConexion();
        Statement statement=connection.createStatement();
        ResultSet resultSet=statement.executeQuery(query);
        //Imprimo por pantalla el resultado de la query
        Debug.imprimirResultadoQuery(resultSet,3);
        //Devuelvo true si la query devolvió al menos un registro
        return resultSet.next();
    }
    //El modelo recibe una referencia al evento que se activará cuando ocurra una excepción
    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }

    //Este método se ejecutará cuando ocurra una excepción
    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    //Codifico el método 'obtenerConexion', que me devolverá una referencia query la base de datos
    //a la cual quiero acceder.
    private Connection obtenerConexion() {
        if (connection == null) {
            try {
                //Registra el driver en la máquina virtual
                Class.forName(driver);
            } catch (ClassNotFoundException ex) {
                reportException(ex.getMessage());
            }
            try {
                //Le paso la dirección de la base de datos, un nombre de usuario, una contraseña
                //y me devuelve una referencia a la base de datos
                String s = this.prefijoConexion + this.ip + "/" + this.bd;
                System.out.println(s);
                System.out.println(usr);
                System.out.println(psw);
                connection = DriverManager.getConnection(s, usr, psw);
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
            Runtime.getRuntime().addShutdownHook(new ShutDownHook());
        }
        //Devuelvo la referencia a la base de datos
        return connection;
    }

    //Cierro la conexión a la base de datos
    private class ShutDownHook extends Thread {

        @Override
        public void run() {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                reportException(ex.getMessage());
            }
        }
    }
}