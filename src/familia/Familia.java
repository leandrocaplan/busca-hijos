
package familia;

public class Familia {
    public static void main(String[] args) {
 //Declaro e instancio el modelo.
        Modelo m = new Modelo();
        //Declaro e instancio la vista, pasandole una referencia al modelo.
        Vista v = new Vista(m);
        //Declaro e instancio el controlador, pasandole una referencia al modelo y a la vista.
        Controlador c = new Controlador(m, v);
        //Llamo al método 'ejecutar' del controlador.
        c.ejecutar();
    }    
}